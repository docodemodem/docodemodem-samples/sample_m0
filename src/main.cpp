/*
 * Sample program for DocodeModem mini
 * Copyright (c) 2020 Circuit Desgin,Inc
 * Released under the MIT license
 */

#include <docodemo.h>
#include <SlrModem.h>

DOCODEMO Dm = DOCODEMO();

#define SEND_PERIOD_MS (10 * 1000)
#define CMD_HEAD 0x01
#define WAIT_PACKET

const uint8_t DEVICE_EI = 55;
const uint8_t DEVICE_GI = 0x02;
const uint8_t DEVICE_DI = 0x00; // 00 means boradcast
const uint8_t CHANNEL = 0x10;

#define SENSOR_MAX_VAL (32.4)
#define SENSOR_MIN_VAL (0.0)

static void doBeep(int count)
{
  Dm.BeepVolumeCtrl(1);

  for (int i = 0; i < count; i++)
  {
    Dm.setPwm(0x03FC);
    vTaskDelay(100);
    Dm.setPwm(0);
    vTaskDelay(50);
  }

  // Dm.setPwm(0x03FC);
  // vTaskDelay(100);
  Dm.setPwm(0);

  Dm.BeepVolumeCtrl(0);
}

TaskHandle_t Handle_main_task;
static void main_task(void *pvParameters)
{
  SerialDebug.println("start main task");

  Dm.begin();

  float sendata[6];

  // modem uart
  UartModem.begin(MLR_BAUDRATE);

  // modem driver
  SlrModem modem;
  modem.Init(UartModem, nullptr);
  Dm.ModemPowerCtrl(ON);
  delay(150); // wait mlr ready

  SlrModemError ret = modem.SetMode(SlrModemMode::LoRaCmd, true);
  if (ret != SlrModemError::Ok)
  {
    doBeep(3);
  }

  modem.SetChannel(CHANNEL, true);
  modem.SetDestinationID(DEVICE_DI, true);
  modem.SetEquipmentID(DEVICE_EI, true);
  modem.SetGroupID(DEVICE_GI, true);

  uint8_t ch, di, gi, ei;
  modem.GetChannel(&ch);
  modem.GetDestinationID(&di);
  modem.GetEquipmentID(&ei);
  modem.GetGroupID(&gi);

  char msg[30];
  sprintf(msg, "ch %d,di %d,ei %d,gi %d", ch, di, ei, gi);
  SerialDebug.println(msg);

  // set packet type and my id
  sendata[0] = (float)(CMD_HEAD << 8 | DEVICE_EI);
  bool sendreq = false;

#ifndef WAIT_PACKET
  portTickType xLastExecuteTime = xTaskGetTickCount();
#else
  long last_count = millis();
#endif

  while (1)
  {
    modem.Work();

#ifndef WAIT_PACKET
    Dm.ModemPowerCtrl(ON);
    delay(150); // wait mlr ready
    sendreq = true;
#else
    if (modem.HasPacket())
    {
      const uint8_t *pData;
      uint8_t len{0};
      modem.GetPacket(&pData, &len);
      SerialDebug.println("rcv");
      if (len == 4) // size check
      {
        uint16_t recvdata[2];
        memcpy(&recvdata[0], pData, len);

        // packet check
        if (recvdata[0] == 0x2 && recvdata[1] == DEVICE_EI)
          doBeep(2);
      }

      modem.DeletePacket();
    }

    long now_count = millis();
    if ((now_count - last_count) >= SEND_PERIOD_MS)
    {
      sendreq = true;
      last_count = now_count;
    }
#endif
    if (sendreq)
    {
      Dm.LedCtrl(GREEN_LED, ON);

      float volt = Dm.readExADC();
      volt *= 10.0;
      volt -= 4.;

      if (volt < 0)
        volt = 0;
      if (volt > 16.)
        volt = 16.;

      float sensorVal = (volt / 16. * (SENSOR_MAX_VAL - SENSOR_MIN_VAL)) + SENSOR_MIN_VAL; // 4-20mA -> SENSOR_MIN_VAL<->SENSOR_MAX_VAL

      sendata[1] = sensorVal;
      // if (sendata[1] < 0)
      //   sendata[1] = 0;
      String msg = "Wind: " + String(sendata[1], 3) + "m/S";
      SerialDebug.println(msg.c_str());

      sendata[2] = Dm.readTemperature();
      sendata[3] = Dm.readPressure() / 100;
      sendata[4] = Dm.readHumidity();
      msg = "Sensor: " + String(sendata[2], 1) + "℃ : " + String(sendata[3], 1) + "hPa : " + String(sendata[4], 1) + " %";
      SerialDebug.println(msg.c_str());

      int16_t rssiCurrentChannel{};
      modem.GetRssiCurrentChannel(&rssiCurrentChannel);
      sendata[5] = rssiCurrentChannel;
      msg = "rssi: " + String(rssiCurrentChannel);
      SerialDebug.println(msg.c_str());

      auto rc = modem.TransmitData((uint8_t *)sendata, sizeof(float) * 6);
      if (rc == SlrModemError::Ok)
      {
        SerialDebug.println("Send Ok");
        doBeep(1);
      }
      else
      {
        SerialDebug.println("Send Ng...");
        Dm.LedCtrl(RED_LED, ON);
        delay(500);
      }

      Dm.LedCtrl(GREEN_LED, OFF);
      sendreq = false;
    }

#ifndef WAIT_PACKET
    Dm.ModemPowerCtrl(OFF);
    Dm.LedCtrl(GREEN_LED, OFF);
    Dm.LedCtrl(RED_LED, OFF);
    vTaskDelayUntil(&xLastExecuteTime, SEND_PERIOD_MS / portTICK_PERIOD_MS);
#endif
  }

  vTaskDelete(NULL);
}

void setup()
{
  SerialDebug.begin(115200);

  xTaskCreate(main_task, "main_task", 1024, NULL, tskIDLE_PRIORITY + 3, &Handle_main_task);

  vTaskStartScheduler();

  // error scheduler failed to start
  // should never get here
  while (1)
  {
    SerialDebug.println("Scheduler Failed! \n");
    delay(1000);
  }
}

void loop()
{
  // never get here
}